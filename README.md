# Tonix
> Outline a brief description of your project.
> Live demo [_here_](https://www.example.com). <!-- If you have the project hosted somewhere, include the link here. -->

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->


## General Information
- Provide general information about your project here.
- What problem does it (intend to) solve?
- What is the purpose of your project?
- Why did you undertake it?
<!-- You don't have to answer all the questions - just the ones relevant to your project. -->


## Technologies Used
- https://github.com/MaxLaumeister/ToneMatrixRedux


## Features
* Playable Grid:
> - Description: 16x16 interactable grid that plays and keeps sound on clicking nodes.  
> - Story: I, Christian Solis, as a software engineer would like to make a step matrix that allows users to easily make music 
    without requiring a vast amount of musical knowledge in order to begin
* Dropdown for scale change
> - Description: Dropdown menu to allow changing scale of the matrix/tones.  
> - Story: I, Rob, as an experienced musician, would like to select different scales to use on my simple step sequencer for
  more advanced time wasting.
* Download mp3
> - Description: Button to save user designated number of repeat iterations.  
> - Story: I, Alejandro Alburjas, As a user of ToneMatrix Redux, I would like to save the music composed as an MP3 file.
* Good harmonizing algorithm
> - Description: Behind-the-scenes algorithm to properly harmonize with currently activated tones.  
> - Story: I, Luke Gayler, as an avid music listener, believe we should have a diverse musical app.
* Simple UI
> - Decription: Easy-to-use UI that a user can intuitively use.  
> - Story: I, Luke Gayler, as a person who likes functional websites, believe we should have a comprehensive website.


## Screenshots
![Tonix Logo](./images/Tonix_Logo.jpg)


## Setup


## Usage


## Project Status
Project is: _in progress_


## Room for Improvement


## Acknowledgements
- This project was inspired by https://github.com/MaxLaumeister/ToneMatrixRedux


## Contact
- Created by Christian Solis, Luke Gayler, Alejandro Alburjas, Seth Petersen, Robert Balthrop.
- Email(in order): cas443@txstate.edu, lmg207@txstate.edu, a_a1218@txstate.edu, srp168@txstate.edu, r_b324@txtstate.edu


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->
